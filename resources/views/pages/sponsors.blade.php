@extends('layouts.app')

@section('pageTitle', 'Sponsors ·')
@section('title', 'Sponsors ·')
@section('description', 'Get things done socially with Taskord.')
@section('image', '')
@section('url', url()->current())

@section('content')
<div class="container-md">
    <div class="card">
        <div class="card-header pt-3 pb-3">
            <span class="h5">Thank you! 💜️</span>
            <div>for supporting our community</div>
        </div>
        <div class="card-body d-flex justify-content-center">
            <div class="col-lg-7">
                <div class="text-center mb-5 pt-4">
                    <div>
                        <img height="80" src="https://i.imgur.com/pJwqa2k.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        AWS (Amazon Web Services) is a comprehensive, evolving cloud computing platform provided by Amazon that includes a mixture of infrastructure as a service (IaaS), platform as a service (PaaS) and packaged software as a service (SaaS) offerings.
                    </div>
                    <div class="mt-2">
                        <a href="https://aws.amazon.com" target="_blank">➜ Go to AWS</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="50" src="https://i.imgur.com/OIscco1.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        DigitalOcean is a cloud hosting provider that offers cloud computing services to business entities so that they can scale themselves by deploying DigitalOcean applications that run parallel across multiple cloud servers without compromising on performance!
                    </div>
                    <div class="mt-2">
                        <a href="https://www.digitalocean.com" target="_blank">➜ Go to DigitalOcean</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="50" src="https://i.imgur.com/dg2XwnG.png" />
                    </div>
                    <div class="task-font h6 mt-5 lh-base">
                        ClickUp is a cloud-based collaboration and project management tool suitable for businesses of all sizes and industries. Features include communication and collaboration tools, task assignments and statuses, alerts and a task toolbar.
                    </div>
                    <div class="mt-2">
                        <a href="https://aws.amazon.com" target="_blank">➜ Go to ClickUp</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="60" src="https://i.imgur.com/bIfKE5i.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        GitLab is a web-based DevOps lifecycle tool that provides a Git-repository manager providing wiki, issue-tracking and continuous integration and deployment pipeline features.
                    </div>
                    <div class="mt-2">
                        <a href="https://gitlab.com" target="_blank">➜ Go to GitLab</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="60" src="https://i.imgur.com/agEjg18.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Gitpod is an online IDE which can be launched from any GitHub/GitLab page within seconds, Gitpod provides you with a fully working development environment, including a VS Code-powered IDE and a cloud-based Linux container configured specifically for the project at hand.
                    </div>
                    <div class="mt-2">
                        <a href="https://gitpod.io" target="_blank">➜ Go to GitPod</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="50" src="https://i.imgur.com/UOlVvAz.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Track errors & monitor performance in all major languages & frameworks with Sentry. Open-source error tracking with full stacktraces & asynchronous context.
                    </div>
                    <div class="mt-2">
                        <a href="https://sentry.io" target="_blank">➜ Go to Sentry</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="60" src="https://i.imgur.com/QI2MTQc.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Speed up your release cycles and deploy bug-free websites and mobile apps with BrowserStack, the industry’s most reliable cloud web and mobile testing platform.
                    </div>
                    <div class="mt-2">
                        <a href="https://www.browserstack.com" target="_blank">➜ Go to BrowserStack</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="50" src="https://i.imgur.com/eSHWnkN.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Todoist is a cloud-based service, so all your tasks and notes sync automatically to any device on which you use the app. You can also use the app offline and have your changes sync later. It's available on every major platform, including Android, iOS, macOS, Windows, Android Wear, and Apple Watch.
                    </div>
                    <div class="mt-2">
                        <a href="https://doist.grsm.io/yogi" target="_blank">➜ Go to Todoist</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="100" src="https://i.imgur.com/VNRFI9v.jpg" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Notion is an application that provides components such as databases, kanban boards, wikis, calendars and reminders. Users can connect these components to create their own systems for knowledge management, note taking, data management, project management, among others.
                    </div>
                    <div class="mt-2">
                        <a href="https://notion.so" target="_blank">➜ Go to Notion</a>
                    </div>
                </div>
                <div class="text-center mb-5 pt-2">
                    <div>
                        <img height="40" src="https://i.imgur.com/Jhg3NXd.png" />
                    </div>
                    <div class="task-font h6 mt-4 lh-base">
                        Uptime Robot is a free tool used to monitor websites . It monitors your websites every 5 minutes and alerts you if your sites are down. It has free uptime monitoring service that supports multiple monitoring types (HTTP, keyword, ping and port).
                    </div>
                    <div class="mt-2">
                        <a href="https://uptimerobot.com" target="_blank">➜ Go to UptimeRobot</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
