# CHANGELOG

- Nov 29, 2020 - **v1.0.1** - https://gitlab.com/taskord/taskord/-/releases/v1.0.1
- Oct 21, 2020 - **v1.0.0** - https://gitlab.com/taskord/taskord/-/releases/v1.0.0
- Oct 13, 2020 - **v0.0.5-alpha** - https://dev.to/taskord/taskord-changelog-oct-13-2bc
- Oct 6, 2020 - **v0.0.4-alpha** - https://dev.to/taskord/taskord-changelog-oct-6-5f4d
- Sep 29, 2020 - **v0.0.3-alpha** - https://dev.to/taskord/taskord-changelog-sep-29-48al
- Sep 22, 2020 - **v0.0.2-alpha** - https://dev.to/taskord/taskord-changelog-sep-22-dbn
- Sep 15, 2020 - **v0.0.1-alpha** - https://dev.to/taskord/taskord-changelog-sep-15-fb1
- Sep 9, 2020 - **v0.0.0-alpha** - First Alpha Release
